#!/usr/bin/python
# A Fancy script to calculate the restoring algorithm dividend on two signed binary numbers
# Shoutout to SimpleFlips

def TWO(a):
    # Function to take the two's complement of a number.
    # Makes sure it's a decimal and then ANDs it with all 1s,
    # (this is a handy way to take the two's complement)
    # then returns the last 8 bits.
    # Can find the two's complement of numbers up to 16 bits
    # but only returns  8 bits.
    a = int(a,2)
    b = format( -a & 0b11111111,'08b' )
    return b

def ADD(a, b):
    # Simple function to add two numbers and return the 
    # binary value of it, removing overflow.
    c = format(int(a,2) + int(b,2),'08b')
    return c[-8:]

def SHIFT(a, b):
    # Function to shift binary values one bit to the right.
    d = str(a) + str(b)
    d = format(int(d,2) << 1,'016b')
    return(d[0:8],d[8:16])

def qCheck(a):
    if a[0] == '1':
        return '0'
    elif a[0] == '0':
        return '1'
    else:
        print("oh no")

def binaryCheck(a, b):
    # Overly complicated function to convert 
    # decimal values into binary values.
    # Negative flag init
    neg = 0
    # turns them to ints, checks if either is negative
    # and sets the flag only if 1 is
    a, b = int(a), int(b)   
    if (a < 0) != (b < 0):
        neg = 1
    # Then returns the absolute value of both in binary 
    # and the negative flag
    a, b = abs(a), abs(b)
    a, b = format(a, '08b'), format(b, '08b')
    return(a, b, neg)


# Taking inputs
decimalDividend = input('Enter the first number: ')
decimalDivisor = input('Enter the second number: ')

# Here you go Dr. Helm
binaryDividend, binaryDivisor, negFlag = binaryCheck(decimalDividend, decimalDivisor)
print("\nNegative Flag: ", negFlag)
# Prints all required values
print(decimalDividend, "=", binaryDividend)
print(decimalDivisor, "=", binaryDivisor)
print(int(decimalDivisor) * -1, "=", TWO(binaryDivisor))
# Set up the equation
print("\n", decimalDividend, "÷", decimalDivisor)
print(binaryDividend, "÷", binaryDivisor, "\n")

# Binary Divisor
binaryM = TWO(binaryDivisor)
# First binary block
binaryA = format(0, '08b')
# Second binary block
binaryQ = binaryDividend
# A literal black space
blankSpace = "        "
# For loop to iterate 8 times for the binary inputs
print("M        A        Q")
for x in range(1, 9):
    # Prints the step so I don't get lost
    print("Step ", x)
    # First comes shift
    # Shift the A and Q left one bit
    print(binaryM, binaryA, binaryQ, "\t Shift")
    binaryA, binaryQ = SHIFT(binaryA, binaryQ)
    print(blankSpace, binaryA, binaryQ)
    # Then comes addition
    # Add the A with the M and store in A
    binarySum = ADD(binaryA, binaryM)
    print(blankSpace, binaryA + "+" + binaryQ + "=" + binarySum)
    # Then comes a bit in the LSB
    # Set LSB of Q to 1 or 0 depending on value of A
    qbit = qCheck(binarySum)
    print(blankSpace, binarySum, binaryQ, "Q ←", qbit)
    binaryQ = binaryQ[0:7] + qbit
    # Then it could end in a messy restore if the A value is negative
    # If the value of A is negative then restore the value of A
    # If not, keep the value of A
    if qbit == '1':
        print(blankSpace, binarySum, binaryQ)
        binaryA = binarySum
    elif qbit == '0':
        print(blankSpace, binarySum, binaryQ, "Restore A")
    
    # Print final value
    print(blankSpace, binaryA, binaryQ)


# Print final value of Q and A as the Remainder and Quotient
print("\n", blankSpace + "Remainder\tQuotient")
print(blankSpace, binaryQ + "\t" + binaryA)
# Print the decimal value of the Remainder and Quotient
decimalRemainder = str(int(binaryA, 2))
decimalQuotient = str(int(binaryQ, 2))
print(blankSpace, decimalRemainder + "\t\t" + decimalQuotient)
# Checks the negative flag
print("\nNegative Flag: ", negFlag)
# Print answer in the format #R#
if negFlag == 1:
    decimalQuotient = "-" + decimalQuotient
print("Answer: ", decimalQuotient + "R" + decimalRemainder)
